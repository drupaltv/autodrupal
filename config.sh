#!/bin/bash


#do not change below
#---

echo "Apache wird für Drupal vorbereitet"
cat <<'EOF' > /etc/apache2/sites-available/000-default.conf
<VirtualHost *:80>
	# The ServerName directive sets the request scheme, hostname and port that
	# the server uses to identify itself. This is used when creating
	# redirection URLs. In the context of virtual hosts, the ServerName
	# specifies what hostname must appear in the request's Host: header to
	# match this virtual host. For the default virtual host (this file) this
	# value is not decisive as it is used as a last resort host regardless.
	# However, you must set it for any further virtual host explicitly.
	#ServerName www.example.com
	ServerAdmin webmaster@localhost
	DocumentRoot /var/www/drupal/web
<Directory /var/www/>
	Options Indexes FollowSymlinks
AllowOverride All	
Require all granted
</Directory>
	# Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
	# error, crit, alert, emerg.
	# It is also possible to configure the loglevel for particular
	# modules, e.g.
	#LogLevel info ssl:warn
	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
	# For most configuration files from conf-available/, which are
	# enabled or disabled at a global level, it is possible to
	# include a line for only one particular virtual host. For example the
	# following line enables the CGI configuration for this host only
	# after it has been globally disabled with "a2disconf".
	#Include conf-available/serve-cgi-bin.conf
</VirtualHost>
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
EOF



echo "Drupal wird installiert"
service apache2 reload && 

echo "Datenbank wird angelegt"
drush -y --root=/var/www/drupal si standard --locale=de --db-su=drupal --db-su-pw=drupal --account-name=admin --account-pass=admin --account-mail=du@beispiel.de --site-mail=website@beispiel.de --site-name=Drupal &&
drush --root="/var/www/drupal" cron >/dev/null 2>&1
cat <<EOT >>  /var/www/drupal/web/sites/default/settings.php

\$settings['trusted_host_patterns'] = [
  '^localhost\$',
];

EOT


drush -y then claro
drush -y --root=/var/www/drupal cset system.theme admin claro
drush -y --root=/var/www/drupal cr


#Cron mittels Drush wird zu .bashrc hinzugefügt, da wsl installationene kein cron ausführen
cd
if [[ $(env |grep WSL_D) ]]; then
echo "/usr/bin/drush --root="/var/www/drupal" cron >/dev/null 2>&1" >> .bashrc
else
echo -n
fi


