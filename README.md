# AUTODRUPAL
## Drupal 9 für Ubuntu und Windows fast von alleine

### Installation

```bash
wget https://gitlab.com/drupaltv/autodrupal/-/raw/master/install; chmod +x install  && ./install

```

### Sieh's dir an, bevor du es selbst ausprobierst

#### Virtualbox
![](autodrupal.png)
#### WSL
[![Drupal Schnellstart Anleitung](http://img.youtube.com/vi/MZ6ZodcK1OI/0.jpg)](http://www.youtube.com/watch?v=MZ6ZodcK1OI "Schnellstartanleitung für Drupal 9")



**Mit diesem Script kannst du [Drupal 9](https://www.drupal.org/9)  in einer leeren Ubuntu Minimalinstallation ausprobieren**


Folgende Schritte musst du ausführen, damit das klappt

1. [Virtualbox installieren](https://www.virtualbox.org/wiki/Downloads)
2. Das [Ubuntu verwenden](https://ubuntu.com/download/thank-you?version=20.04&architecture=amd64) 
3. Virtualbox starten
4. Eine virtuelle Maschine anlegen
5. Das Ubuntu  ISO einlegen
6. Der Installation folgen, bis die Maschine neu gestartet wird
7. Ein Terminal öffnen
8.  Den Code oben einfügen
#### Zugangsdaten nach der Installation

Da es noch kein Iso gibt, dass das Skript von selbst installiert, sind die Zugangsdaten für das System, die du während der Ubuntu Minimalinstallation angibst.

Mysql-Datenbankbenutzer:    drupal
--
Mysql-Datanbank-Passwort:   drupal
--
Mysql Datanbank:             drupal
--
Drupal ist über http://localhost mit den folgenden Zugangsdaten zu erreichen:
--
Benutzername: admin
--
Passwort: admin 
--


#### 64 Bit ONLY
