#!/bin/bash


#drupal core  version. change here to update to Drupal 9
db=drupal
user=drupal
pw=drupal
ver=11.1.12
dir=/home/*/autodrupal
#xrandrscript
cp xr /usr/local/bin/xr
#Root-Check
if [[ $EUID -ne 0 ]]; then
   echo "Dieses Skript muss als Superuser ausgeführt wrden." 
   echo "Verwenden Sie sudo  oder wenden Sie sich an Ihren Systemadministrator!"
   
   exit 1
fi

clear

#System aktuallisieren, falls von iso installiert wurde.
wget -qO - https://packagecloud.io/AtomEditor/atom/gpgkey | sudo apt-key add -
sh -c 'echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main" > /etc/apt/sources.list.d/atom.list' &&
apt update  && apt -y full-upgrade
clear &&
apt -y install mariadb-server &&
/etc/init.d/mysql start&>  /home/$SUDO_USER/autodrupal.log  &&

#dependency checks
if [[ $(env |grep WSL_D) ]]; then
wget https://launchpad.net/~rafaeldtinoco/+archive/ubuntu/lp1871129/+files/libc6_2.31-0ubuntu8+lp1871129~1_amd64.deb
dpkg --install libc6_2.31-0ubuntu8+lp1871129~1_amd64.deb
apt-mark hold libc6
 apt -y --fix-broken install
 apt -y full-upgrade
else
echo "Abhängigkeiten für Virtualbox-Gäste werden installiert"
apt-get -y -q install virtualbox-guest-dkms virtualbox-guest-utils
 apt -y full-upgrade
fi

#install atom
apt-get -y -q install atom aspell-de hunspell-de-de
#drupal stack

apt-get -y -q install apache2 apache2-utils php-fpm php-bz2 php-mbstring git unzip php-opcache php-mysql php-gd openssl php-xml php-dev php-xmlrpc php-json php-curl php-zip curl unzip composer git bzip2 openssl php-uploadprogress &&
service php*-fpm start
service  apache2 start


#Apache und Mysql verbinden
a2enmod proxy_fcgi setenvif rewrite && a2enconf php*-fpm   &&


#drupal installieren
export COMPOSER_ALLOW_SUPERUSER=1 
echo "Vorbereitung abgeschlossen. Drupal wird installiert"
#composer create-project drupal/recommended-project:"$ver" /var/www/drupal --no-interaction &&
composer create-project drupal/cms /var/www/drupal --no-interaction &&
#trusted host auf localhost setzen 

#composer isntallieren
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php --quiet &&
mv composer.phar /usr/bin/composer &&
rm composer-setup.php &&

chown -Rh $lognname:www-data /var/www &&
chmod -R 775 /var/www
cd /var/www/drupal;composer require drush/drush && 
cd /opt
composer require drupal/console;cd /home/*
ln -s /var/www/drupal/vendor/bin/drush /usr/bin/drush
ln -s /opt/vendor/drupal/console/bin/drupal /usr/bin/drupal

cd $dir
#Schreib- und Leserechte festlegen

#Kommandozeilenwerkzeuge ausführbar machen
echo 'export PATH="/var/www/drupal/vendor/bin:$PATH"' >> /home/*/.bashrc

#Dienste für wsl start
echo "Notwendige Dienste werden gestartet"
service php*-fpm start  &&
service apache2 start  &&
#mysql startet während der drupal installation, um die Kompatibilität zur WSL zu gewährleisten.


#Cron mittels Drush wird zu .bashrc hinzugefügt, da wsl installationene kein cron ausführen

if [[ $(env |grep WSL_D) ]]; then
 
echo "/usr/bin/drush --root="/var/www/drupal" cron >/dev/null 2>&1" >> /home/*/.bashrc
else
echo -n
fi


source /home/*/.bashrc &&
rm -- "$0"
exit
